//
//  mainViewController.swift
//  RssAlbumsApp
//
//  Created by Venkatesh Ventrapragada on 8/12/19.
//  Copyright © 2019 Venkatesh Ventrapragada. All rights reserved.
//

import UIKit

class MediaCellView: UITableViewCell {
    var name : UILabel!
    var artist : UILabel!
    var icon: UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .white
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //Clear Previous Image
        icon.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func SetCellData(data: NSDictionary) {
        
        // Create UI
        name = CreateLabel()
        artist = CreateLabel()
        icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(icon)
        
        //Set textView text
        name.text = data.object(forKey: "name") as? String
        artist.text = data.object(forKey: "kind") as? String
        
        //Set image
        let imageURL = data.object(forKey: "artworkUrl100") as! String
        if let url = URL(string: imageURL) {
            DownloadImage(url: url)
        }
        
        // Set layout of subviews
        SetLayout()
    }
    
    //Download image
    func DownloadImage(url :URL) {
        GetDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.icon.image = UIImage(data: data)
            }
        }
    }
    
    func GetDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    //Create label
    func CreateLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label)
        return label
    }
    
    func SetLayout() {
        icon.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        icon.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        icon.leadingAnchor.constraint(equalTo: self.leadingAnchor , constant: 10).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        name.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        name.leadingAnchor.constraint(equalTo: icon.trailingAnchor , constant: 10).isActive = true
        name.trailingAnchor.constraint(equalTo: self.trailingAnchor , constant: -10).isActive = true
        name.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
        
        artist.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        artist.leadingAnchor.constraint(equalTo: icon.trailingAnchor , constant: 10).isActive = true
        artist.trailingAnchor.constraint(equalTo: self.trailingAnchor , constant: -10).isActive = true
        artist.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 10).isActive = true
        artist.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
    }
}

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource{
    
    var pickerView: UIPickerView!
    var pickerData : NSMutableArray = []
    var mediaTypeTF : UITextField!
    var feedTypeTF : UITextField!
    var activeTF : UITextField!
    var list: NSDictionary?
    var mediaList : NSMutableArray = []
    var mediaTableView: UITableView!
    var tableData : NSMutableArray = []
    var activityIndicator : UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add observer to recieve data
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(UpdateFeedList), name:NSNotification.Name(rawValue: "UpdateFeedListNotification"), object: nil)
        
        // Set type of media and thier feed
        if let path = Bundle.main.path(forResource: "MediaList", ofType: "plist") {
            let dict = NSDictionary(contentsOfFile: path)
            mediaList = dict?.object(forKey: "MediaList") as! NSMutableArray
            list = dict?.object(forKey: "FeedList") as? NSDictionary
        }
        
        //Create UI
        pickerView = CreatePickerView()
        mediaTypeTF = CreateTextField()
        mediaTypeTF.inputView = pickerView
        mediaTypeTF.inputAccessoryView = CreateToolBar()
        
        feedTypeTF = CreateTextField()
        feedTypeTF.inputView = pickerView
        feedTypeTF.inputAccessoryView = CreateToolBar()
        
        mediaTableView = CreateTableView()
        activityIndicator = CreateActivityIndicator()
        
        //Set Default Value
        mediaTypeTF.text = mediaList[0] as? String
        let feedList : NSArray = list?.object(forKey: mediaTypeTF.text as Any) as! NSArray
        feedTypeTF.text = feedList[0] as? String
        
        LayoutSubView()
        FetchRequest()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTF = textField
        UpdatePickerData(textField: textField)
        return true
    }
    
    func UpdatePickerData(textField : UITextField) {
        // Update picker list
        pickerData.removeAllObjects()
        if textField == mediaTypeTF {
            pickerData.addObjects(from: mediaList as! [Any])
        }
        else
        {
            let feedList : NSArray = list?.object(forKey: mediaTypeTF.text as Any) as! NSArray
            pickerData.addObjects(from: feedList as! [Any])
        }
        pickerView.reloadAllComponents()
    }
    
    //Create textfield
    func CreateTextField() -> UITextField {
        let textField = UITextField()
        textField.delegate = self
        textField.font = UIFont.systemFont(ofSize: 20)
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(textField)
        return textField
    }
    
    //Create pickerview
    func CreatePickerView() -> UIPickerView {
        let pickerView = UIPickerView()
        pickerView.backgroundColor = UIColor.black
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        return pickerView
    }
    
    //Create toolbar for pickerview
    func CreateToolBar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.black
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(DonePicker))
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        return toolBar
    }
    
    //Create tableview
    func CreateTableView() -> UITableView {
        let tableView = UITableView()
        tableView.register(MediaCellView.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        return tableView
    }
    
    //Create activity indicator
    func CreateActivityIndicator() -> UIActivityIndicatorView{
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.color = UIColor.black
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        self.view.addSubview(indicator)
        return indicator
    }
    
    // Pickerview delegate and datasource functions
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string:(pickerData[row] as? String)!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        return attributedString
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        //Update selected media or feed type
        if mediaTypeTF == activeTF{
            mediaTypeTF.text = pickerData[row] as? String
            
            let feedList : NSArray = list?.object(forKey: mediaTypeTF.text as Any) as! NSArray
            feedTypeTF.text = feedList[0] as? String
        }
        else{
            feedTypeTF.text = pickerData[row] as? String
        }
    }
    
    
    @objc func DonePicker() {
        activeTF.resignFirstResponder()
        FetchRequest()
    }
    
    func FetchRequest() {
        activityIndicator.startAnimating()
        let service = WebService()
        service.FeedSettings(mediaType: mediaTypeTF.text! as NSString, feedType: feedTypeTF.text! as NSString)
    }
    
    // Update tebleview with new data
    @objc func UpdateFeedList(_ notification: Notification){
        DispatchQueue.main.async{
            self.activityIndicator.stopAnimating()
            self.tableData.removeAllObjects()
            let result = notification.userInfo?["FeedList"]
            let array = result as! NSArray
            self.tableData.addObjects(from: array as! [Any])
            self.mediaTableView.reloadData()
        }
    }
    
    // tableview delegate and datasource functions
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MediaCellView = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! MediaCellView
        let data = tableData[indexPath.row] as! NSDictionary
        //Set cell with new data
        cell.SetCellData(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = tableData[indexPath.row] as! NSDictionary
        
        let controller = DetailViewController()
        self.present(controller, animated: true, completion: nil)
        controller.SetData(data: data)
    }
    
    func LayoutSubView() {
        mediaTypeTF.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 60).isActive = true
        mediaTypeTF.leadingAnchor.constraint(equalTo: self.view.leadingAnchor , constant: 10).isActive = true
        mediaTypeTF.widthAnchor.constraint(greaterThanOrEqualToConstant: 80).isActive = true
        mediaTypeTF.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        feedTypeTF.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 60).isActive = true
        feedTypeTF.trailingAnchor.constraint(equalTo: self.view.trailingAnchor , constant:-10).isActive = true
        feedTypeTF.heightAnchor.constraint(equalToConstant: 40).isActive = true
        feedTypeTF.leadingAnchor.constraint(equalTo: mediaTypeTF.trailingAnchor, constant: 20).isActive = true
        
        mediaTableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 110).isActive = true
        mediaTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor , constant: 10).isActive = true
        mediaTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor , constant:-10).isActive = true
        mediaTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

