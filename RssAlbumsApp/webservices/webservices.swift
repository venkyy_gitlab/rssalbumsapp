//
//  webservices.swift
//  RssAlbumsApp
//
//  Created by Venkatesh Ventrapragada on 8/12/19.
//  Copyright © 2019 Venkatesh Ventrapragada. All rights reserved.
//

import UIKit

class WebService: NSObject {
    func FeedSettings(mediaType: NSString, feedType: NSString)
    {
        //Create URL
        var url: String = "https://rss.itunes.apple.com/api/v1/us/apple-music/coming-soon/all/10/explicit.json"
        url = url.lowercased()
        url = url.replacingOccurrences(of: " ", with: "-", options: .regularExpression)
        
        guard let URL = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: URL)
        urlRequest.httpMethod = "GET"
        
        // Create session for URL
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            var result : NSArray = []
            if let error = error {
                print ("error: \(error)")
            }
            else
            {
                //Checking response status
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode == 200 {
                    if let data = data {
                        do {
                            //Converting response data into string
                            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableLeaves) as? [String : Any]
                            
                            if let responseDict = json!["feed"] as? NSDictionary
                            {
                                result = responseDict.object(forKey: "results")as! NSArray
                                print(result)
                            }
                        }catch {
                            print(error)
                        }
                    }
                }
            }
            // Notifying observers
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"UpdateFeedListNotification"), object: nil, userInfo:["FeedList":result])
        })
        task.resume()
    }
}
