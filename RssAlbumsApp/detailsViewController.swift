//
//  detailsViewController.swift
//  RssAlbumsApp
//
//  Created by Venkatesh Ventrapragada on 8/12/19.
//  Copyright © 2019 Venkatesh Ventrapragada. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    var icon: UIImageView!
    var name: UILabel!
    var artist: UILabel!
    var genre: UILabel!
    var releaseDate: UILabel!
    var copyrightInfo: UILabel!
    var backButton: UIButton!
    var url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
    }
    
    //Create label
    func CreateLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.textColor = .black
        label.backgroundColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        return label
    }
    
    //Download image
    func DownloadImage(url :URL) {
        GetDataFromUrl(url: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async() {
                self.icon.image = UIImage(data: data)
            }
        }
    }
    
    func GetDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func SetData(data: NSDictionary) {
        // Create UI
        name = CreateLabel()
        name.text = "Name: "
        artist = CreateLabel()
        artist.text = "Artist: "
        genre = CreateLabel()
        genre.text = "Genre: "
        releaseDate = CreateLabel()
        releaseDate.text = "Release Date: "
        copyrightInfo = CreateLabel()
        copyrightInfo.text = "Copyright Info"
        
        icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        icon.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(icon)
        
        backButton = UIButton()
        backButton.setTitle("back", for: .normal)
        backButton.backgroundColor = .gray
        backButton.translatesAutoresizingMaskIntoConstraints = false
        backButton.addTarget(self, action: #selector(JumpToMainViewController), for: .touchDown)
        self.view.addSubview(backButton)
        
        //Set label text
        name.text = data.object(forKey: "name") as? String
        artist.text = data.object(forKey: "kind") as? String
        genre.text = data.object(forKey: "genres") as? String
        releaseDate.text = data.object(forKey: "releasedate") as? String
        copyrightInfo.text = data.object(forKey: "copyrightinfo") as? String
        url = data.object(forKey: "url") as! String
        
        let imageURL = data.object(forKey: "artworkUrl100") as! String
        if let url = URL(string: imageURL) {
            DownloadImage(url: url)
        }
        LayoutSubView()
    }
    
    @objc func JumpToMainViewController() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    public func LayoutSubView() {
        
        icon.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 60).isActive = true
        icon.leadingAnchor.constraint(equalTo: self.view.leadingAnchor , constant: 20).isActive = true
        icon.trailingAnchor.constraint(equalTo: self.view.trailingAnchor , constant: -20).isActive = true
        icon.bottomAnchor.constraint(equalTo: name.topAnchor, constant: -20).isActive = true
        
        name.bottomAnchor.constraint(equalTo: artist.topAnchor, constant: -10).isActive = true
        name.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        name.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        name.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        artist.bottomAnchor.constraint(equalTo: genre.topAnchor, constant: -10).isActive = true
        artist.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        artist.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        artist.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        genre.bottomAnchor.constraint(equalTo: releaseDate.topAnchor, constant: -10).isActive = true
        genre.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        genre.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        genre.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        releaseDate.bottomAnchor.constraint(equalTo: copyrightInfo.topAnchor, constant: -10).isActive = true
        releaseDate.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        releaseDate.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        releaseDate.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        copyrightInfo.bottomAnchor.constraint(equalTo: backButton.topAnchor, constant: -10).isActive = true
        copyrightInfo.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        copyrightInfo.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        copyrightInfo.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        backButton.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -20).isActive = true
        backButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        backButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
}
